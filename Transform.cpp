/**
 * @file   Transform.cpp
 * @Author Muhammet Emin Kurt email=mekurt55@gmail.com
 * @date   December 2021

 *
*/
#include"Transform.h"
#include <iomanip>
/**
	@brief:			Bu fonksiyon kurucu fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
Transform::Transform()
{

}
/**
	@brief:			Bu fonksiyon ile aci degerleri alinir.
	@param:			Parametre olarak vector3d ang degeri alinir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void Transform::Setangles(Eigen::Vector3d newang)
{
	Angles = newang;
}
/**
	@brief:			Bu fonksiyon ile taban koordinat sistemi noktalari alinir.
	@param:			Parametre olarak vector3d tr degeri alinir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void Transform::Settrans(Eigen::Vector3d tr)
{
	Trans = tr;
}
/**
	@brief:			Bu fonksiyon angles degerlerini dondurur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
Eigen::Vector3d Transform::Getangles()
{
	return Angles;
}
/**
	@brief:			Bu fonksiyon trans degerlerini dondurur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
Eigen::Vector3d Transform::Gettrans()
{

	return Trans;
}
/**
	@brief:			Bu fonksiyon ile rotation matrixi olusturulur.
	@param:			Parametre olarak vector3d ang degeri alinir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void Transform::Setrotation(Eigen::Vector3d ang)
{
	Transmatrix(0, 0) = cos(ang[2])*cos(ang[1]);
	Transmatrix(0, 1) = cos(ang[2])*sin(ang[1])*sin(ang[0]) - sin(ang[2]) * cos(ang[0]);
	Transmatrix(0, 2) = cos(ang[2])*sin(ang[1])*cos(ang[0]) + sin(ang[2]) * sin(ang[0]);
	Transmatrix(1, 0) = sin(ang[2])*cos(ang[1]);
	Transmatrix(1, 1) = sin(ang[2])*sin(ang[1])*sin(ang[0]) + cos(ang[2]) * cos(ang[0]);
	Transmatrix(1, 2) = sin(ang[2])*sin(ang[1])*cos(ang[0]) - cos(ang[2]) * sin(ang[0]);
	Transmatrix(2, 0) = -sin(ang[1]);
	Transmatrix(2, 1) = cos(ang[1])*sin(ang[0]);
	Transmatrix(2, 2) = cos(ang[1])*cos(ang[0]);
}
/**
	@brief:			Bu fonksiyon ile rotation matrix testi olusturulur.
	@param:			Parametre olarak matrix4d trmtx degeri alinir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void Transform::Setrottest(Eigen::Matrix4d newtr)
{
	Transmatrix = newtr;
}
/**
	@brief:			Bu fonksiyon ile rotation matrix testi dondurulur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
Eigen::Matrix4d Transform::Getrottest()
{
	return Transmatrix;
}
/**
	@brief:			Bu fonksiyon ile transform matrix testi olusturulur.
	@param:			Parametre olarak matrix4d trx degeri alinir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void Transform::Settranslationtest(Eigen::Matrix4d newtrx)
{
	Transmatrix = newtrx;
}
/**
	@brief:			Bu fonksiyon ile transform matrix testi dondurulur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
Eigen::Matrix4d Transform::Gettranslationtest()
{
	return Transmatrix;
}
/**
	@brief:			Bu fonksiyon ile translation vector3d matrixi olusturulur.
	@param:			Parametre olarak vector3d tr degeri alinir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void Transform::Settranslation(Eigen::Vector3d newtr)
{

	Transmatrix(0, 3) = newtr[0];
	Transmatrix(1, 3) = newtr[1];
	Transmatrix(2, 3) = newtr[2];
	Transmatrix(3, 0) = 0;
	Transmatrix(3, 1) = 0;
	Transmatrix(3, 2) = 0;
	Transmatrix(3, 3) = 1;

}
/**
	@brief:			Bu fonksiyon ile alinan bir adet point icin transform donusumu yapilir.
	@param:			Parametre olarak Point p degeri alinir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
Point Transform::Dotransform(Point np)
{
	Point Transpoint;
	double sum = 0;
	double resultp[4][1], newp[4][1];
	newp[0][0] = np.getX();
	newp[1][0] = np.getY();
	newp[2][0] = np.getZ();
	newp[3][0] = 1;


	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 1; j++) {
			for (int k = 0; k < 4; k++) {
				sum += (Transmatrix(i, k) * newp[k][j]);
			}
			resultp[i][j] = sum;
			sum = 0;
		}
	}

	Transpoint.setXYZ(resultp[0][0], resultp[1][0], resultp[2][0]);

	return Transpoint;
}
/**
	@brief:			Bu fonksiyon ile alinan point icin transform donusumu yapilir. List dondurur.
	@param:			Parametre olarak list<Point>lp degeri alinir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
list<Point> Transform::Dotransform(list<Point>newlp)
{
	/*list<Point> transformedList;
	list<Point>::iterator iter;
	for (iter = lp.begin(); iter != lp.end(); iter++)
	{
		transformedList.push_back(doTransform(*iter));
	}
	return transformedList;*/
	list<Point> transformedpoint;
	list<Point>::iterator iter;
	Point temp;
	Eigen::Vector4d pointb, pointa, tempmatrix;
	//iter = lp.begin();

	for (iter = newlp.begin(); iter != newlp.end(); iter++)
	{
		pointb(0) = iter->getX();
		pointb(1) = iter->getY();
		pointb(2) = iter->getZ();
		pointb(3) = 1;
		tempmatrix = Transmatrix * pointb;
		temp.setXYZ(tempmatrix(0), tempmatrix(1), tempmatrix(2));
	

		transformedpoint.push_back(temp);
	}

	return transformedpoint;
}
/**
	@brief:			Bu fonksiyon ile alinan Pointcloud icin transform donusumu yapilir. Pointcloud dondurur.
	@param:			Parametre olarak Pointcloud pc degeri alinir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
PointCloud Transform::Dotransform(PointCloud newpc)
{
	PointCloud Transpointcloud;
	list<Point>P;
	P = newpc.getPoints();
	Transpointcloud.setPoints(Dotransform(P));
	

	return Transpointcloud;
}
/**
	@brief:			Bu fonksiyon yikici fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
Transform::~Transform()
{

}