/**
* @file       PointCloudFilter.h
* @author     Yasin Binler  e-mail: yasinbinler06@gmail.com
* @date       29 Aral�k 2021 Carsamba
* @brief      Bu kod parcacigi derinlik kamerasi fonksiyonlarini gerceklemeye yarar.
*/
#pragma once
#include"PointCloud.h"
class PointCloudFilter
{
public:
	PointCloudFilter();
	~PointCloudFilter();
	virtual void Filters(PointCloud&) = 0;
	
};

