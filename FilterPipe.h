/**
*    @file     FilterPipe.h
*	 @Author   Sude Nur Ayaz (ssudeayazz@gmail.com)
*    @date     January, 2022
*	 @brief    Bu kod parcacigini derinlik kamerasi fonksiyonlarini calst�rmak i�in kullandim.
*/
#pragma once
#include<vector>
#include"PointCloudFilter.h"
class FilterPipe
{
private:
	vector<PointCloudFilter*> filters;
public:
	FilterPipe();
	~FilterPipe();
	void addFilter(PointCloudFilter*);
	void filterOut(PointCloud&);
};

