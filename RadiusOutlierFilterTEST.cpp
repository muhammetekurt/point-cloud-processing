/*
* @file RadiusOutlierTest.cpp
* @Author Sude Nur Ayaz (ssudeayazz@gmail.com)
* @date December, 2021
* @brief Bu test fonksiyonu DepthCamera Classini calistirir ve kontrol eder.
*/

#include <iostream>
#include "DepthCamera.h"
#include "Point.h"
#include "PointCloud.h"
#include <string>
#include"RadiusOutlierFilter.h"

using namespace std;

int main() {
	DepthCamera camera;
	PointCloud pcloud;

	pcloud = camera.capture();

	RadiusOutlierFilter rot;

	cout << endl << "Before the filter point number: ";
	cout << pcloud.getPointNumber() << endl;

	rot.filter(pcloud);

	cout << endl << "Before the filter point number: ";
	cout << pcloud.getPointNumber() << endl;

	system("pause");

}
