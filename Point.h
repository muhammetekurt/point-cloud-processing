/**
*  @file Point.h
*  @Author Betul AKSU (aksubb000@gmail.com)
*  @date December, 2021
*  @brief This class holds the coordinates of 3D points in the point cloud.
*/
#pragma once
#include<math.h>
#include<cstdio>
#include<cstdlib>
#include<iostream>

using namespace std;
class Point
{
private:
	double x;
	double y;
	double z;
public:
	Point();
	~Point();
	Point(double, double, double);
	void setXYZ(double , double , double );
	double getX() const;
	double getY() const;
	double getZ() const;
	bool operator==(const Point P) const;
	double distance(const Point&)const;
};

