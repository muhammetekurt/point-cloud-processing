/**
* @file       PointCloudFilter.cpp
* @author     Yasin Binler  e-mail: yasinbinler06@gmail.com
* @date       29 Aral�k 2021 Carsamba
* @brief      Headerda olu�turulan fonksiyonlari isleyisini icerir.
*/
#include "PointCloudFilter.h"
/**
	@brief:			Bu fonksiyon yapici fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
PointCloudFilter::PointCloudFilter()
{

}
/**
	@brief:			Bu fonksiyon yikici fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
PointCloudFilter::~PointCloudFilter()
{

}