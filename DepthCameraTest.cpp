/**
* @file DepthCameraTest.cpp
* @Author Sude Nur Ayaz (ssudeayazz@gmail.com)
* @date December, 2021
* @brief Bu test fonksiyonu DepthCamera Classini calistirir ve kontrol eder.
* BU ATILCAK
*/
#include"PointCloud.h"
#include"Point.h"
#include"DepthCamera.h"
#include<iostream>
using namespace std;

int main() {

	DepthCamera camera;
	PointCloud a;
	PointCloud b;

	camera.setfileName("camera1.txt");
	a = camera.capture();

	vector<Point>x, y;
	vector<Point>::iterator i1, i2;

	x = a.getPoints();
	i1 = x.begin();

	cout << "camera1 ";
	cout << endl;
	cout << endl;



	for (i1 = x.begin(); i1 != x.end(); i1++) {
		cout << (*i1).getX() << " ";
		cout << (*i1).getY() << " ";
		cout << (*i1).getZ() << " ";
	}

	camera.setfileName("camera2.txt");
	b = camera.capture();

	i1 = y.begin();

	cout << "camera2 ";
	cout << endl;
	cout << endl;

	for (i1 = y.begin(); i1 < y.end(); i1++) {
		cout << (*i1).getX() << " ";
		cout << (*i1).getY() << " ";
		cout << (*i1).getZ() << " ";


	}


	system("pause");
}
