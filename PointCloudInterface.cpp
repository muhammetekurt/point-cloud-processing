/**
* @file		PointCloudInterface.cpp
* @Author	�zge K�branur OMAR	 e-mail=(kubraomar.8@gmail.com)
* @date		January 2022
*/
#include "PointCloudInterface.h"
/**
	@brief:			Bu kurucu fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
PointCloudInterface::PointCloudInterface()
{
}

/**
	@brief:			Bu yikici fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
PointCloudInterface::~PointCloudInterface()
{
}
/**
	@brief:			Bu fonksiyon ile PointCloudGenerator tarafindan yaratilan nesneler generators'un icine atilir.
	@param:			Parametre olarak PointCloudGenerator* g degeri alinir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void PointCloudInterface::Addgenerator(PointCloudGenerator* neww)
{
	Generators.push_back(neww);
}
/**
	@brief:			Bu fonksiyon PointCloudRecorder dan alinan nesne ile islem gormus noktari dosyaya kaydeder.
	@param:			Parametre olarak PointCloudRecorder* s degeri alinir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void PointCloudInterface::Setrecorder(PointCloudRecorder* s)
{
	this->Recorder = s;
}
/**
	@brief:			Fonksiyonu generators �yesinde bulunan t�m nesnelerden captureFor fonksiyonu �a�r�larak nokta bulutlar� sa�lan�p patch'e atilir.
					Daha sonra her bir nokta bulutu pointCloud �yesine eklenir.
					K�saca, proje K�s�m 1 �de yapt���n� uygulamadaki a�amalar bu tek fonksiyon ile ger�ekle�tirilir
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
bool  PointCloudInterface::Generate()
{
	for (int i = 0; i < Generators.size(); i++)
	{
		Patch = Generators[i]->captureFor();
		Pointcloud = Pointcloud + Patch;
	}
	return true;
}
/**
	@brief:			Bu fonksiyon pointCloud i�indeki noktalar� dosyaya kaydeder.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
bool  PointCloudInterface::Record()
{
	string str = "output.txt";
	Recorder->Setfilename(str);
	Recorder->Save(Pointcloud);
		return true;
	
}