/**
/**
* @file PointCloudTest.cpp
* @Author Sude Nur Ayaz (ssudeayazz@gmail.com)
* @date January, 2021
* @brief Tests the PointCloud class
*/
#include<iostream>
#include<string>
#include<math.h>
#include<vector>
#include<iterator>
#include "Point.h"
#include "PointCloud.h"

using namespace std;

int main(void)
{
	vector<Point> pointVector1, pointVector2, pointVector4;
	vector<Point>::iterator iterator1, iterator2, iterator3;
	Point point;

	PointCloud pointCloud1, pointCloud2, pointCloud4;
	pointCloud1.setPointNumber(3);
	pointCloud2.setPointNumber(3);

	for (int i = 0; i < 3; i++)
	{
		point.setXYZ(i, i + 1, i + 2);
		pointVector1.push_back(point);
	}

	pointCloud1.setPoints(pointVector1);
	pointVector1 = pointCloud1.getPoints();


	cout << "pointVector1:" << endl;
	for (iterator1 = pointVector1.begin(); iterator1 != pointVector1.end(); iterator1++)
	{
		cout << iterator1->getX() << " " << iterator1->getY() << " " << iterator1->getZ() << endl;
	}

	pointVector2 = pointVector1;
	
	cout << endl << "pointVector2:" << endl;
	for (iterator2 = pointVector2.begin(); iterator2 != pointVector2.end(); iterator2++)
	{
		cout << iterator2->getX() << " " << iterator2->getY() << " " << iterator2->getZ() << endl;
	}

	pointCloud4 = pointCloud1 + pointCloud2;
	pointVector4 = pointCloud4.getPoints();

	cout << endl << "Toplami:" << endl;
	for (iterator3 = pointVector4.begin(); iterator3 != pointVector4.end(); iterator3++)
	{
		cout << iterator3->getX() << " " << iterator3->getY() << " " << iterator3->getZ() << endl;
	}
	

	return 0;
}