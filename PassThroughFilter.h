/**
* @file       PassThroughFilter.h
* @author     Yasin Binler  e-mail: yasinbinler06@gmail.com
* @date       29 Aral�k 2021 Carsamba
* @brief      This code defines the member functions that will be used in filtering.
*/
#pragma once
#include"PointCloud.h"
#include"PointCloudFilter.h"

class PassThroughFilter :public PointCloudFilter
{
	double uLX;
	double lLX;
	double uLY;
	double lLY;
	double uLZ;
	double lLZ;
public:
	PassThroughFilter();
	~PassThroughFilter();
	void setUX(double);
	void setLX(double);
	void setUY(double);
	void setLY(double);
	void setUZ(double);
	void setLZ(double);
	double getUX();
	double getLX();
	double getUY();
	double getLY();
	double getUZ();
	double getLZ();
	void Filters(PointCloud&);
};

