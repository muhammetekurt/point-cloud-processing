/**
* @file PointTest.cpp
* @Author Betul AKSU (aksubb000@gmail.com)
* @date January, 2021
* @brief Tests the Point class
*/


#include <iostream>
#include <string>
#include "Point.h"
using namespace std;

int main()
{
	Point P;
	Point A;
	P.setXYZ(1.1, 2.5, 4.6);
	A.setXYZ(1, 2, 4);

	cout <<" Nokta1: ";
	cout << P.getX()<<" , ";
	cout << P.getY() << " , ";
	cout << P.getZ() << endl;
	cout << " Nokta2: ";
	cout << A.getX() << " , ";
	cout << A.getY() << " , ";
	cout << A.getZ() << endl<<endl;

	if (P == A)
	{
		cout << "iki nokta birbirine esittir." << endl<<endl;
	}
	else
	{
		cout << "iki nokta birbirine esit degildir." << endl<<endl;
	}



	cout << "iki nokta arasindaki mesafe = " << P.distance(A) << endl<<endl;

	system("pause");

}