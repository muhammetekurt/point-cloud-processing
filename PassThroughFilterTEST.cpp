#include "DepthCamera.h"
#include "PointCloud.h"
#include "PassThroughFilter.h"
#include<iostream>
#include<string>
#include<list>
#include<iterator>

using namespace std;
/**
* @file					PassThroughFilterTEST.cpp
* @author				Yasin Binler  e-mail: yasinbinler06@gmail.com
* @date					29 Aral�k 2021 Carsamba
*/
int main() {
	PassThroughFilter ptf;
	PointCloud pcloud1, pcloud2;
	DepthCamera Cam;
	vector<Point> tmp;
	vector<Point>::iterator it;

	string str = "camera1.txt";
	Cam.setfileName(str);
	pcloud1 = Cam.capture();
	ptf.filter(pcloud1);

	int i = 0;
	tmp = pcloud1.getPoints();
	cout << "\n\nFor camera1.txt" << endl << endl << endl;

	for (it = tmp.begin(); it != tmp.end(); it++)
	{
		cout << i + 1 << " -> " << it->getX() << " " << it->getY() << " " << it->getZ() << endl;
		i++;
	}

	string str2 = "camera2.txt";
	Cam.setfileName(str2);
	pcloud2 = Cam.capture();

	ptf.set_upper_x(500);
	ptf.set_upper_y(500);

	ptf.filter(pcloud2);

	vector<Point> p;

	p = pcloud2.getPoints();

	i = 0;
	cout << "\n\nFor camera2.txt\n\n";
	for (it = tmp.begin(); it != tmp.end(); it++)
	{
		cout << i + 1 << " -> " << it->getX() << " " << it->getY() << " " << it->getZ() << endl;
		i++;
	}
	system("pause");
}