/**
 * @file   PointCloudRecorder.h
 * @Author Muhammet Emin Kurt email=mekurt55@gmail.com
 * @date   December 2021
 @brief kamera dosylarından okunan ,filtrelendikten sonra olan nokta bulutlarını dosyaya yazar.
 *
*/

#pragma once
#include<string>
#include<iostream>
#include"PointCloud.h"
using namespace std;
class PointCloudRecorder
{
	string filename;
public:

	PointCloudRecorder();  
	void Setfilename(string);
	string Getfilename();
	bool Save(PointCloud&);
	~PointCloudRecorder();
};

