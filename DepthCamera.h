/**
* @file		DepthCamera.h
* @Author   Sude Nur Ayaz (ssudeayazz@gmail.com)
* @date     January, 2022
* @brief	Bu classta dosyadan numaralar okunur ve daha �nceden olu�turdu�umuz diziye atan�r.
*/
#pragma once
#include<iostream>
#include <fstream>
#include <iomanip>
#include<string>
#include"PointCloud.h"
#include"Point.h"
#include"PassThroughFilter.h"
#include "RadiusOutlierFilter.h"
#include"PointCloudGenerator.h"

using namespace std;
class DepthCamera :public PointCloudGenerator
{
	string fileName;
public:
	DepthCamera();
	~DepthCamera();
	void setDept(string);
	string getDept();
	PointCloud capture();
	PointCloud captureFor();
	
};

